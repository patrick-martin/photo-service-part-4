import React from "react";
import { getImages, postImage } from "../clientApi";

class Home extends React.Component {
  state = {
    getImages: { imageURIs: [], statusCode: 0 },
    postImage: {
      imageURI: "",
      statusCode: 0
    }
  };

  componentDidMount() {
    getImages().then(result => {
      this.setState({ ...this.state, getImages: result });
    });
  }

  handleImageUpload = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    //would call an action creator, and pass form data into action creator
    //..which would make a fetch request
    postImage(formData).then(result => {
      this.setState({ ...this.state, postImage: result });
    });
  };

  render() {
    return (
      <>
        <h1>Kenziegram</h1>
        <form onSubmit={this.handleImageUpload}>
          <input type="file" name="picture" />
          <button type="submit">Upload Image</button>
        </form>

        <p>
          {this.state.postImage.statusCode === 200 &&
            "Your upload was successful!"}
        </p>

        {this.state.getImages.imageURIs.map(uri => (
          <img key={uri} src={uri} />
        ))}
      </>
    );
  }
}

export default Home;
