//import fs from "fs";
//import { getImagesResponse, postImageResponse } from "../apiStubs";
//models represent code that persists our data objects
//this is where we want to get our image data to SEND BACK to controllers
//apiStubs moving out of 'controllers' and into 'models'

import admin from "firebase-admin";
//import serviceAccount from "./firebaseAdminServiceAccount.json";  //already converted to JS object when imported
import { DefaultTransporter } from "google-auth-library";

//const serviceAccount = JSON.parse(process.env.FIREBASE_ADMIN_SERVICE_ACCOUNT)  //tried this method with setting up .env to include this variable

//Is this not right?  Should we not be passing this into our admin.credentials, since it will contain our private key
const serviceAccount = JSON.parse(process.env.FIREBASE_ADMIN_SERVICE_ACCOUNT);
// const serviceAccount = {
//   type: process.env.TYPE,
//   project_id: process.env.PROJECT_ID,
//   private_key_id: process.env.PRIVATE_KEY_ID,
//   private_key: process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, "\n"),
//   client_email: process.env.CLIENT_EMAIL,
//   client_id: process.env.CLIENT_ID,
//   auth_uri: process.env.AUTH_URI,
//   token_uri: process.env.TOKEN_URI,
//   auth_provider_x509_cert_url: process.env.AUTH_PROVIDER_x509_CERT_URL,
//   client_x509_cert_url: process.env.CLIENT_x509_CERT_URL
// };
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "kenziegram-32297.appspot.com"
});
const bucket = admin.storage().bucket();

// () -> Promise<for an Array of<URI's>
export const find = () => {
  return bucket
    .getFiles()
    .then(data => {
      const files = data[0];
      const fileURLs = files.map(file => {
        return file.getSignedUrl({
          action: "read",
          expires: Date.now() + 1000 * 60 * 60 //expires 1hr later
        });
      });
      return Promise.all(fileURLs);
    })
    .then(fileURLs => {
      return fileURLs.map(fileURL => fileURL[0]);
    });
};

export const create = buffer => {
  const timestamp = Date.now();
  const file = bucket.file(String(timestamp));
  return file.save(buffer).then(() => {
    return file
      .getSignedUrl({
        action: "read",
        expires: Date.now() + 10000 * 60 * 60 //expires 1hr
      })
      .then(fileURL => fileURL[0]);
  });
};

export default { find, create };

//Environment Variables for .env file:

// TYPE="service_account"
// PROJECT_ID="kenziegram-32297"
// PRIVATE_KEY_ID="4fd13dbaa332671af90ff39e8169facd4250844d"
// PRIVATE_KEY="-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDdpvxTggtH/l0s\nGYgB4K6Kqm7I2xMZRGohIKXb1pHpNwH78ZLWhK5VGPPrEubF07hzDvfYjIlXMvwF\nWlNqRWPIHk0p/iv0Vzhbrb5MrDkDQa7Y5ZMeZ31/WNoWZLuxUBoD24SxPgi7Rijp\no4JOeimTzen57OpImh4aJw1uyaCJ/s/J0U5FwKTjhw467hGaPYMbT0sNiRYwPent\nKd/rD5OwlrmSFeoZnZiGueYPylEy3whkY7PlFSUcvuqoOU8/7YZuWDuvOi22co9R\nVNWbdBFjHltn7BQffEHkoUnxaUVrMn2QMIt0Knebf+00YmbCiKT2uoF3GrobSVjb\nqFI1NiBpAgMBAAECggEAM8ekvqVsmT706S51tWqVm9NA2i9DgO2YlXCZDohLwd2V\n2xOOKAsK5tpTcSETRDe3qTlf8PizxOkmMuHtBrB7JO7fO+Yiwov1oIUwJZ0+HGKl\nxAmtmra2fJ93IzAZIM0Dps8LapSBWa6ENEB8WnTzbZH3DOmpuZg3XAivfKghltq+\nviXRmRR94tbyTs5lH9fgSXphfxjjZGaJUGd/vdz4MY09+fKOtZZUnEb6Oju98v4Y\nQ0E19Dia0F+Zo6QFOT3vBoapWOkaPpzhauWJjxsyvXgdyVFjpC43FjQw/76gwux5\nLXWZdC3E4+39HFSVOJJaYPV+dGbaXSXD8D649LTqOQKBgQD0lK+IYvkU+dNUW5FJ\nwYlkEE2Nn2afS6e0rwXrnNrsMGGMNjErG7BHZ86+KuWvraQVJbfubAIiFgws/401\nhBqf86ytLMj4e8O31zWCKeFE2apu5fV5fUyIbkce/RoNEo6/9+VFlGhp8AiXik//\nlozIJuc+3MRMoehffaqrFzP0SwKBgQDoAEAb/AbisYnndVbDII3ceGsiKrVvR61a\n0OeWtiHTTSajQZlbOz4cI1+PRvI0lkBp6Jl94fkcOag9YybRpQfEmDRwDQTnHO8d\n+hb1MVoM1cl7P5glsYFefhblOno34rOoSxixbYhQdXnKvXk2OgDUXMlFUD3El06y\nTWR3rBtFmwKBgAR+RstGpjTQciV2mTC1sYR5GDOl3gjHJwmWvZEajJsSOHiO9lBI\nbfCh45Zyy01y7zTEQKDMsy1VIlwhRcLZ7nNJ0r5LuFV4UJwsA/ukMVNGfG8VInFc\n7+8Bt6TGGZhr4tt7/mZM1646jq3aCKY1adizfkQVYjXWM6KteFk0T+wJAoGAGovk\nyg71jrz35ac0jVFNSjvxLlCqwBf4ixq6VTFz/2SlliqsbJcDzV5y0ccxcbj7l+LI\npXHU+ljSmmudBoIWuHUeEvA6bmOOLOTv+ZBzGGLiEhs9wppkDjbpUcN2NqzXF9u3\n8J1y6pn2nK8/jmfmrmYLlcXPu44AsEBQzazA1h8CgYAZL+37YEq6Lm2ctczHnyhp\ni2JCWi1Hcyar5Z39jCr/K0zL0KmjNGPBpiBY5ovindnSHths91eKFfwSWMSLaWRH\n/q5xzbxXzrto5xO6WT0PNv+lezfofaaKOZBiMx7/CpRDqxQUX1zYerDu3Leor4MK\nW8dYbw02TGWX5dF3QweaPw==\n-----END PRIVATE KEY-----\n"
// CLIENT_EMAIL="firebase-adminsdk-jgh74@kenziegram-32297.iam.gserviceaccount.com"
// CLIENT_ID="108324198645545094558"
// AUTH_URI="https://accounts.google.com/o/oauth2/auth"
// TOKEN_URI="https://oauth2.googleapis.com/token"
// AUTH_PROVIDER_X509_CERT_URL="https://www.googleapis.com/oauth2/v1/certs"
// CLIENT_X509_CERT_URL="https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-jgh74%40kenziegram-32297.iam.gserviceaccount.com"

// // () -> Promise<Array<URI: String>>
// const find = () => {
//   return fs.promises
//     .readdir(process.env.RAZZLE_PUBLIC_DIR + "/images")
//     .then(fileNames => {
//       return fileNames.map(fileName => "/images/" + fileName);
//     });

//   //   return new Promise((resolve, reject) => {
//   //     resolve(getImagesResponse.imageURIs);
//   //   });
// };

// //fsPromises.readdir
// //need to include (Buffer) as param
// //buffer will be found in req.file.buffer
// //remember, req.file will show the request
// //fs.promises.writeFile
// const create = buffer => {
//   const timestamp = Date.now();
//   return fs.promises
//     .writeFile(process.env.RAZZLE_PUBLIC_DIR + "/images/" + timestamp, buffer)
//     .then(() => {
//       return "/images/" + timestamp;
//     });
//   //   return new Promise((resolve, reject) => {
//   //     resolve(postImageResponse.imageURI);
//   //   });
// };

//this JS object represents our Image model

//Firebase Admin SDK Key:
