import multer from "multer";
//import { getImagesResponse, postImageResponse } from "../apiStubs";
import { Image } from "../models";
//this is just stub/dummy data
//we will be talking to a third-party api and sending back actual data

//controller module has to match the contract specificed in spec.yaml
//response must have imageURI's/statusCode
//these are specifically for handling responses to requests!
//sending data out to the models
//collecting data from request, sending it where it needs to go
//collecting data for response, sending that out

//using memoryStorage (also could use diskStorage)

const upload = multer({ storage: multer.memoryStorage() });

export const getImages = (req, res) => {
  console.log("getImages");
  Image.find().then(uris => res.send({ imageURIs: uris, statusCode: 200 }));
};

//where in the form data should we look(keyname = "picture"; demo it was "image")
export const postImage = [
  upload.single("picture"),
  (req, res) => {
    console.log("postImage");
    console.log(req.file); //request body format should have a buffer?  Says it is undefined..
    Image.create(req.file.buffer).then(
      uri => res.send({ imageURI: uri, statusCode: 200 }) //check spec.yaml for properties
    );
  }
];
