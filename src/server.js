import { App } from "./components";
import React from "react";
import { StaticRouter } from "react-router-dom";
import express from "express";
import { renderToString } from "react-dom/server";
import apiRouter from "./apiRouter";
//import swaggerDocsRouter from "./swaggerDocsRouter"; //can get rid of this
//import { getImages, postImage } from "./controllers/images"; //and this..

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

//created instance of server
//server can be set up to respond at certain endpoints
//.use() -> setting up middleware
const server = express();
server
  .disable("x-powered-by")
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  //.get("/api/images", getImages) //can get rid of this, I believe
  //.post("/api/images", postImage) //and this
  //.use("/api", swaggerDocsRouter) //and this..
  .use("/api", apiRouter) //INSTEAD, we use this...
  .get("/*", (req, res) => {
    const context = {};
    const markup = renderToString(
      <StaticRouter context={context} location={req.url}>
        <App />
      </StaticRouter>
    );

    if (context.url) {
      res.redirect(context.url);
    } else {
      res.status(200).send(
        `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>Welcome to Razzle</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${
          assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ""
        }
        ${
          process.env.NODE_ENV === "production"
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
    </head>
    <body>
        <div id="root">${markup}</div>
    </body>
</html>`
      );
    }
  });

export default server;
